import logging
from typing import Any, Optional, Union

from openpyxl import load_workbook
from openpyxl.styles import Alignment, Border, Side
from openpyxl.styles.named_styles import NamedStyle

_logger = logging.getLogger(__name__)


class ExcelEditor:
    """
    Classe permettant d'accéder au fonction de base d'openpyxl pour éditer un fichier
    excel existant
    """

    def __init__(self, filename: str):

        self.filename = filename
        self.workbook = load_workbook(filename=filename)
        self.feuille = self.workbook.active

    def sauvegarder(self, path: str | None = None):
        """
        Sauvegarde le Workbook

        Args:
            /
        Returns:
            None
        Raises:
            TypeError("Workbook is read-only")
        """
        self.workbook.save(self.filename)

    def ajouter_titre(self, titre: str):
        """
        Ajoute un titre à la feuille active

        Examples:
            Wb.ajouter_titre("feuille 1")
        Args:
            titre
        Returns:
            None
        Raises:
            ValueError("le titre n'est pas valide"))
        """
        if titre and type(titre) is str:
            self.workbook.active.title = titre
        else:
            raise ValueError("le titre n'est pas valide")

    def get_cellule(self, coordinates: str) -> Any:
        """
        retourne le contenu d'une cellule appelée au format [""]

        Examples:
            Wb.get_cellule("A8")
        Args:
            coordinates : les coordonnées de la cellule
        Returns:
            Any : le contenu de la cellule
        Raises:
            ValueError("Coordonnées non valides"))
        """
        try:
            return self.feuille[coordinates].value
        except ValueError as e:
            logging.exception(e)

    def get_feuilles_liste(self) -> 'list[str]':
        """
        retourne le contenu le nom des feuilles excel

        Examples:
            Wb.get_feuilles_liste()
        Args:
            titre
        Returns:
            None
        Raises:
            ValueError("le titre n'est pas valide"))
        """
        return self.workbook.sheetnames

    def inserer_lignes(self, idx: int, amount: int = 1):
        """
        insert une certaine quantité de lignes avant la ligne d'indice idx

        Examples:
            Wb.inserer_lignes(5, 2) ajoute 2 lignes avant l'ancienne 5e ligne
        Args:
            idx : l'indice de la ligne avant laquelle on insert les nouvelles lignes
            amount: le nombre de ligne à insérer
        Returns:
            None
        Raises:
            /
        """
        self.feuille.insert_rows(idx=idx, amount=amount)

    def inserer_cols(self, idx: int, amount: int = 1):
        """
        insert une certaine quantité de colonnes avant la colonne d'indice idx

        Examples:
            Wb.inserer_cols(5, 2) ajoute 2 colonnes avant l'ancienne 5e colonne
        Args:
            idx : l'indice de la colonne avant laquelle on insert les nouvelles colonnes
            amount: le nombre de colonnes à insérer
        Returns:
            None
        Raises:
            /
        """
        self.feuille.insert_cols(idx=idx, amount=amount)

    def supprimer_lignes(self, idx: int, amount: int = 1):
        """
        supprime une certaine quantité de ligne après la ligne d'indice idx inclus

        Examples:
            Wb.supprimer_lignes(5, 2) supprime 2 lignes avant l'ancienne 5e ligne
        Args:
            idx : l'indice de la ligne à partir de laquelle on supprime les lignes
            amount: le nombre de lignes à insérer
        Returns:
            None
        Raises:
            /
        """
        self.feuille.insert_rows(idx=idx, amount=amount)

    def supprimer_cols(self, idx: int, amount: int = 1):
        """
        supprime une certaine quantité de colonnes après la colonne d'indice idx inclus

        Examples:
            Wb.supprimer_cols(5, 2) supprime 2 colonnes après l'ancienne 5e colonne
        Args:
            idx : l'indice de la colonne après laquelle on supprime les colonnes
            amount: le nombre de colonnes à insérer
        Returns:
            None
        Raises:
            /
        """
        self.feuille.delete_cols(idx=idx, amount=amount)

    def formater_cellule(self, coordinates: str, data: Any, style: NamedStyle | str) -> None:
        """
        Formate le contenu d'une cellule avec le style voulu à partir d'un NamedStyle

        Examples:
            from openpyxl.styles import NamedStyle, Font, Border, Side
            highlight = NamedStyle(name="highlight")
            highlight.font = Font(bold=True, size=20)
            bd = Side(style='thick', color="000000")
            highlight.border = Border(left=bd, top=bd, right=bd, bottom=bd)
            ws.formater_cellule('A1', 45, highlight)
            # ou avec un style builtin
            # ws.formater_cellule('A1', 45, 'Headline 1')

        Args:
            coordinates (str): coordonnées de la cellule
            data (Any): les données à mettre dans la cellule
            style (NamedStyle): le style à ajouter
        """

        self.feuille[coordinates].style = style
        self.feuille[coordinates] = data
