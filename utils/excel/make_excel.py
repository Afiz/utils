# todo : remplace la librairie xlwt par la librairie openpyxl

import logging
import os

import xlwt
from PIL import ImageFont

_logger = logging.getLogger(__name__)

"""
 Styles
     liste des couleurs : https://docs.google.com/spreadsheets/d/1ihNaZcUh7961yU7db1-Db0lbws4NT24B7koY8v8GHNQ/pubhtml?gid=1072579560&single=true

     border:
        'no_line':  0x00,
        'thin':     0x01,
        'medium':   0x02,
        'dashed':   0x03,
        'dotted':   0x04,
        'thick':    0x05,
        'double':   0x06,
        'hair':     0x07,
        'medium_dashed':                0x08,
        'thin_dash_dotted':             0x09,
        'medium_dash_dotted':           0x0a,
        'thin_dash_dot_dotted':         0x0b,
        'medium_dash_dot_dotted':       0x0c,
        'slanted_medium_dash_dotted':   0x0d,
"""
border = 'medium'

style_none = xlwt.easyxf(
    """ alignment: horizontal left,
                                          wrap 1;
                         """
)

style_defaut = xlwt.easyxf(
    """ alignment: horizontal right,
                                          vertical center;
                           """
)

style_border = xlwt.easyxf(
    """ borders:   left {0},
                                          right {0},
                                          top {0},
                                          bottom {0},
                                          top_color black;
                               alignment: horizontal left,
                                          wrap 1,
                                          vertical center;
                           """.format(
        border
    )
)

style_border_centre = xlwt.easyxf(
    """ borders:   left {0},
                                                 right {0},
                                                 top {0},
                                                 bottom {0},
                                                 top_color black;
                                      alignment: horizontal center,
                                                 wrap 1,
                                                 vertical center;
                                  """.format(
        border
    )
)

style_gris = xlwt.easyxf(
    """ borders:   left {0},
                                         right {0},
                                         top {0},
                                         bottom {0},
                                         top_color black;
                              pattern:   pattern solid,
                                         fore_color gray25;
                              alignment: horizontal left,
                                         wrap 1,
                                         vertical center;
                            """.format(
        border
    )
)

style_gris_centre = xlwt.easyxf(
    """ borders:   left {0},
                                                right {0},
                                                top {0},
                                                bottom {0},
                                                top_color black;
                                     pattern:   pattern solid,
                                                fore_color gray25;
                                     alignment: horizontal center,
                                                wrap 1,
                                                vertical center;
                                   """.format(
        border
    )
)

style_gris_right = xlwt.easyxf(
    """ borders:   left {0},
                                                right {0},
                                                top {0},
                                                bottom {0},
                                                top_color black;
                                     pattern:   pattern solid,
                                                fore_color gray25;
                                     alignment: horizontal right,
                                                wrap 1,
                                                vertical center;
                                   """.format(
        border
    )
)

style_vert = xlwt.easyxf(
    """ borders:   left {0},
                                         right {0},
                                         top {0},
                                         bottom {0},
                                         top_color black;
                              pattern:   pattern solid,
                                         fore_color bright_green;
                              alignment: horizontal left,
                                         wrap 1,
                                         vertical center;
                            """.format(
        border
    )
)


style_vert_centre = xlwt.easyxf(
    """ borders:   left {0},
                                                right {0},
                                                top {0},
                                                bottom {0},
                                                top_color black;
                                     pattern:   pattern solid,
                                                fore_color bright_green;
                                     alignment: horizontal center,
                                                wrap 1,
                                                vertical center;
                                   """.format(
        border
    )
)

style_vert_right = xlwt.easyxf(
    """ borders:   left {0},
                                                right {0},
                                                top {0},
                                                bottom {0},
                                                top_color black;
                                     pattern:   pattern solid,
                                                fore_color bright_green;
                                     alignment: horizontal right,
                                                wrap 1,
                                                vertical center;
                                   """.format(
        border
    )
)

style_border_rouge = xlwt.easyxf(
    """
                              borders:   left {0},
                                         right {0},
                                         top {0},
                                         bottom {0},
                                         top_color black;
                              pattern:   pattern solid,
                                         fore_color red;
                              alignment: horizontal left,
                                         wrap 1,
                                         vertical center;
                            """.format(
        border
    )
)

style_border_rouge_centre = xlwt.easyxf(
    """
                                     borders:   left {0},
                                                right {0},
                                                top {0},
                                                bottom {0},
                                                top_color black;
                                     pattern:   pattern solid,
                                                fore_color red;
                                     alignment: horizontal center,
                                                wrap 1,
                                                vertical center;
                                   """.format(
        border
    )
)

style_border_rouge_right = xlwt.easyxf(
    """
                                     borders:   left {0},
                                                right {0},
                                                top {0},
                                                bottom {0},
                                                top_color black;
                                     pattern:   pattern solid,
                                                fore_color red;
                                     alignment: horizontal right,
                                                wrap 1,
                                                vertical center;
                                   """.format(
        border
    )
)

style_border_orange = xlwt.easyxf(
    """
                              borders:   left {0},
                                         right {0},
                                         top {0},
                                         bottom {0},
                                         top_color black;
                              pattern:   pattern solid,
                                         fore_color orange;
                              alignment: horizontal left,
                                         wrap 1,
                                         vertical center;
                            """.format(
        border
    )
)

style_border_orange_centre = xlwt.easyxf(
    """
                                     borders:   left {0},
                                                right {0},
                                                top {0},
                                                bottom {0},
                                                top_color black;
                                     pattern:   pattern solid,
                                                fore_color orange;
                                     alignment: horizontal center,
                                                wrap 1,
                                                vertical center;
                                   """.format(
        border
    )
)

style_border_orange_right = xlwt.easyxf(
    """
                                     borders:   left {0},
                                                right {0},
                                                top {0},
                                                bottom {0},
                                                top_color black;
                                     pattern:   pattern solid,
                                                fore_color orange;
                                     alignment: horizontal right,
                                                wrap 1,
                                                vertical center;
                                   """.format(
        border
    )
)

style_border_gras = xlwt.easyxf(
    """
                               font:      bold 1;
                               borders:   left {0},
                                          right {0},
                                          top {0},
                                          bottom {0},
                                          top_color black;
                               alignment: horizontal right,
                                          vertical center;
                            """.format(
        border
    )
)

style_label = xlwt.easyxf(
    """ borders:   left {0},
                                          right {0},
                                          top {0},
                                          bottom {0},
                                          top_color black;
                               alignment: horizontal left,
                                          vertical center;
                            """.format(
        border
    )
)

style_titre = xlwt.easyxf(
    """ font:      height 250,
                                         bold 1;
                              alignment: horizontal center,
                                         wrap 1,
                                         vertical center;
                            """
)


style_titre1 = xlwt.easyxf(
    """ font:     height 400,
                                         bold 1;
                              alignment: horizontal center,
                                         wrap 1,
                                         vertical center;
                            """
)

style_titre2 = xlwt.easyxf(
    """font:      height 250,
                                         bold 1;
                              alignment: horizontal left,
                                         wrap 1,
                                         vertical center;
                            """.format(
        border
    )
)

style_titre3 = xlwt.easyxf(
    """font:      bold 1;
                              alignment: horizontal center,
                                         wrap 1,
                                         vertical center;
                              borders:   left {0},
                                         right {0},
                                         top {0},
                                         bottom {0},
                                         top_color black;
                             """.format(
        border
    )
)


class MakeExcel:
    """
    Permet de completer ou de crée un fichier excel à partir d'un dictionnaire correctement structuré.
    ce dictionnaire peut entre composé de dictionnaires et de tableaux
    le dict final est composé des elements suivant :
        - pour une cellule
            nom        fonction                                                                        remarque
            'x'        position de la ligne dans le fichier excel (la premiere = 0)                    obligatoire
            'y'        position de la colonne dans le fichier excel (la premiere = 0)                  obligatoire
            'x2'       position de la fin de ligne dans le fichier excel en cas de merge de cellule
            'y2'       position de la fin de ligne dans le fichier excel en cas de merge de cellule    obligatoire si x2 est defini
            'autosize' calcul automatique de la hauteur de la cellule paramètre : (taille des cellules)
            'style'    style de la cellule voir ci-dessus.
                       Si image definie 'valeur' contient le chemin de l'image.
            'valeur'   valeur de la cellule                                                            obligatoire
        - pour la taille d'un ligne ou colonne
            nom        fonction                                                                        remarque
            'size'     'col' ou 'ligne' ou 'zoom'                                                      obligatoire
            'num'      numéro de la colonne ou de la ligne                                             obligatoire
            'valeur'   taille des cellules                                                             obligatoire
                       ou valeur de zoom pour l'impression en pourcentage

    exemple :
    entete = {
         'titre':            {'x': 0, 'y': 0, 'x2': 0, 'y2':5, 'style':'titre', 'valeur': u"Mon titre"},
         'annee_label':      {'x': 1, 'y': 0, 'style':'label',  'valeur': u"Année"},
         'annee':            {'x': 1, 'y': 1, 'style':'border', 'valeur': 0},
         'semaine_label':    {'x':2, 'y': 0, 'style':'label',  'valeur': u"Semaine"},
         'semaine':          {'x':2, 'y': 1, 'style':'border', 'valeur': 0},
         'ministere_label':  {'x':3, 'y': 0, 'style':'label',  'valeur': u"Ministère"},
         'ministere':        {'x':3, 'y': 1, 'style':'border', 'valeur': 0},
        }
    """

    def __init__(self, filename, valeurs, options=[]):
        self.options = options

        try:
            self.font = ImageFont.truetype('/opt/odoo/modules/mlcn/fonts/arial.ttf', 10)
        except:
            current_dir = os.getcwd()
            current_dir = current_dir[0 : current_dir.rfind('/')]
            self.font = ImageFont.truetype('/' + current_dir + '/modules/mi_app_mlcn/fonts/arial.ttf', 10)

        workbook = xlwt.Workbook()
        sheet = workbook.add_sheet('fiches')
        sheet.portrait = 0  # impression paysage
        # sheet.fit_num_pages = 1
        self.parcourt_dict(sheet, valeurs)

        workbook.save(filename)

    def parcourt_dict(self, sheet, dictionaire):
        for valeur in dictionaire.values():
            if isinstance(valeur, dict):
                # print "{} is dict".format(valeur)
                self.parcourt_dict(sheet, valeur)
            elif isinstance(valeur, list):
                for item in valeur:
                    if isinstance(item, dict):
                        # print "{} is dict".format(item)
                        self.parcourt_dict(sheet, item)
                    else:
                        self.write(sheet, dictionaire)
            else:
                self.write(sheet, dictionaire)
                break

    def write(self, sheet, dictionaire):
        if 'size' in dictionaire:
            if dictionaire['size'] == 'col':
                # logger.info("col:{} => size={}".format(chr(ord('A') + dictionaire['num']), dictionaire['valeur']))
                # Default value of width is 2962 units and excel points it to as 8.11 units. Hence i am multiplying 367 to length of data.
                sheet.col(dictionaire['num']).width = dictionaire['valeur'] * 367
            elif dictionaire['size'] == 'row':
                sheet.row(dictionaire['num']).height_mismatch = True
                sheet.row(dictionaire['num']).height = dictionaire['valeur'] * 256
            elif dictionaire['size'] == 'zoom':
                sheet.print_scaling = dictionaire['valeur']

        elif 'x' in dictionaire:
            style = style_defaut
            if 'style' in dictionaire:
                if dictionaire['style'] == 'titre':
                    style = style_titre
                elif dictionaire['style'] == 'titre1':
                    style = style_titre1
                elif dictionaire['style'] == 'titre2':
                    style = style_titre2
                elif dictionaire['style'] == 'titre3':
                    style = style_titre3
                elif dictionaire['style'] == 'border':
                    style = style_border
                elif dictionaire['style'] == 'border_centre':
                    style = style_border_centre
                elif dictionaire['style'] == 'borderGras':
                    style = style_border_gras
                elif dictionaire['style'] == 'label':
                    style = style_label
                elif dictionaire['style'] == 'gris':
                    style = style_gris
                elif dictionaire['style'] == 'gris_right':
                    style = style_gris_right
                elif dictionaire['style'] == 'gris_centre':
                    style = style_gris_centre
                elif dictionaire['style'] == 'vert':
                    style = style_vert
                elif dictionaire['style'] == 'vert_right':
                    style = style_vert_right
                elif dictionaire['style'] == 'vert_centre':
                    style = style_vert_centre
                elif dictionaire['style'] == 'border_rouge':
                    style = style_border_rouge
                elif dictionaire['style'] == 'border_rouge_right':
                    style = style_border_rouge_right
                elif dictionaire['style'] == 'border_rouge_centre':
                    style = style_border_rouge_centre
                elif dictionaire['style'] == 'border_orange':
                    style = style_border_orange
                elif dictionaire['style'] == 'border_orange_right':
                    style = style_border_orange_right
                elif dictionaire['style'] == 'border_orange_centre':
                    style = style_border_orange_centre
                elif dictionaire['style'] == 'none':
                    style = style_none
                elif dictionaire['style'] == 'image':
                    if 'scale_x' in dictionaire:
                        scale_x = dictionaire['scale_x']
                    else:
                        scale_x = 0.3
                    if 'scale_y' in dictionaire:
                        scale_y = dictionaire['scale_y']
                    else:
                        scale_y = 0.125
                    sheet.insert_bitmap(dictionaire['valeur'], dictionaire['x'], dictionaire['y'], x=0, y=0, scale_x=scale_x, scale_y=scale_y)
                    return

            val = dictionaire['valeur']
            if isinstance(val, bool):
                if val is True:
                    val = 'Oui'
                else:
                    val = 'Non'
            elif isinstance(val, int):
                if '0_to_empty' in self.options:
                    if val == 0:
                        val = ''
            elif isinstance(val, list):
                val = len(val)
            elif val is None:
                val = ''

            if 'autosize' in dictionaire:
                width = dictionaire['autosize'] * 10
                # print u"width={}".format(width)
                text_split = val.split('\n')
                adjusted_height = 0
                for text in text_split:
                    length = (self.font.getsize(text)[0] * 130) / 100
                    adjusted_height += (length / width) + 1
                    # print u"{}, {} , {}".format(text, adjusted_height, length)

                font_height = 256
                if 'style' in dictionaire:
                    if dictionaire['style'] == 'titre':
                        font_height = 300

                if sheet.row(dictionaire['x']).height < adjusted_height * font_height:
                    sheet.row(dictionaire['x']).height = adjusted_height * font_height

            if 'x2' in dictionaire:
                """print u"x={} y= {} x2={} y2= {} valeur = {}".format(dictionaire['x'],
                dictionaire['y'],
                dictionaire['x2'],
                dictionaire['y2'],
                dictionaire['valeur'])"""
                sheet.write_merge(dictionaire['x'], dictionaire['x2'], dictionaire['y'], dictionaire['y2'], val, style)
            else:
                # print u"x={} y= {} valeur = {}".format(dictionaire['x'], dictionaire['y'],dictionaire['valeur'])
                sheet.write(dictionaire['x'], dictionaire['y'], val, style)


def decale_y(dictionaire, decale_y):
    if type(dictionaire) == list:
        for _dictionaire_ in dictionaire:
            _decale_y_(_dictionaire_, decale_y)
    else:
        _decale_y_(dictionaire, decale_y)


def _decale_y_(dictionaire, valeur_decale_y):
    for valeur in dictionaire.values():
        if isinstance(valeur, dict):
            _decale_y_(valeur, valeur_decale_y)
        elif isinstance(valeur, list):
            for item in valeur:
                if isinstance(item, dict):
                    # print "{} is dict".format(item)
                    _decale_y_(item, valeur_decale_y)
                else:
                    _set_decale_y_(valeur, valeur_decale_y)
        else:
            _set_decale_y_(dictionaire, valeur_decale_y)
            break


def _set_decale_y_(dictionaire, valeur_decale_y):
    if 'y' in dictionaire:
        dictionaire['y'] += valeur_decale_y
        if 'y2' in dictionaire:
            dictionaire['y2'] += valeur_decale_y
    elif 'size' in dictionaire:
        if dictionaire['size'] == 'col':
            dictionaire['num'] += valeur_decale_y


def suppression_ligne(dictionaire, ligne_supprimer):
    if type(dictionaire) == list:
        for _dictionaire_ in dictionaire:
            _suppression_ligne_(_dictionaire_, ligne_supprimer)
    else:
        _suppression_ligne_(dictionaire, ligne_supprimer)


def _suppression_ligne_(dictionaire, ligne_supprimer):
    for valeur in dictionaire.values():
        if isinstance(valeur, dict):
            _suppression_ligne_(valeur, ligne_supprimer)
        elif isinstance(valeur, list):
            for item in valeur:
                if isinstance(item, dict):
                    # print "{} is dict".format(item)
                    _suppression_ligne_(item, ligne_supprimer)
                else:
                    _set_suppression_ligne_(valeur, ligne_supprimer)
        else:
            _set_suppression_ligne_(dictionaire, ligne_supprimer)
            break


def _set_suppression_ligne_(dictionaire, ligne_supprimer):
    if 'x' in dictionaire:
        if dictionaire['x'] == ligne_supprimer:
            print('effacement ligne %d %s' % (ligne_supprimer, dictionaire))
            del dictionaire
            return
        elif dictionaire['x'] > ligne_supprimer:
            dictionaire['x'] -= 1

        if 'x2' in dictionaire:
            if dictionaire['x2'] > ligne_supprimer:
                dictionaire['x2'] -= 1
