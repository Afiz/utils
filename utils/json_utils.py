import datetime


def test_key_good_format(data, key, _type, vide_interdit=True, absent_interdit=True, size_max=1024, size_min=0):
    """
    Test le format d'une clé d'un json

    :param data: json à tester
    :param key: cle à tester
    :param _type: type de données
    :param vide_interdit: si True la cle doit ne doit pas être à none, ni une string vide
    :param absent_interdit: si True la cle doit être presente
    :param size_max: taille maximale d'une chaine de caractére
    :param size_min: taille minimale d'une chaine de caractére
    :return: None en cas de reussite ou un message d'erreur en cas d'echec
    """

    if key in data:
        contenu = data[key]
        if type(contenu) is not _type:
            if vide_interdit and contenu is None:
                return f'le champ {key} ne doit pas être vide'
            return f"le champ {key} n'est pas au bon format. reçu = {type(contenu)}, attendu = {_type}"
        if type(contenu) is str:
            if vide_interdit:
                if contenu == '':
                    return f'le champ {key} ne doit pas être vide'
            if len(contenu) > size_max:
                return f'le champ {key} est trop long ({len(contenu)} pour un max de {size_max})'
            if len(contenu) < size_min:
                return f'le champ {key} est trop court ({len(contenu)} pour un min de {size_min})'
    else:
        if absent_interdit:
            return f'le champ {key} doit être présent'
    return None


def convert_all_datetime(data, formatime='%d/%m/%Y'):
    """
    Conversion des types datetime.datetime en chaine de caractéres
    :param data: json à convertir
    :param formatime: format de la chaine desirée (https://docs.python.org/3/library/datetime.html#strftime-strptime-behavior)
    :return: json converti
    """
    if type(data) == list:
        tab = []
        for val in data:
            tab.append(convert_all_datetime(val, formatime))
        return tab
    elif type(data) == dict:
        dic = {}
        for key, val in data.items():
            dic.update({key: convert_all_datetime(val, formatime)})
        return dic
    elif type(data) == datetime.datetime:
        return data.strftime(formatime)
    else:
        return data
