# reference: https://docs.python.org/fr/3/library/smtplib.html
#            https://docs.python.org/fr/3/library/email.message.html
# Import smtplib for the actual sending function
# For guessing MIME type based on file name extension
import mimetypes
import os
import smtplib
from email.message import EmailMessage
from email.utils import make_msgid

import json_utils

# mail
mail_host = 'icasso22.mel75.si.mi'
mail_port = 25

if 'FDR_COURRIEL_HOST' in os.environ:
    mail_host = os.environ['FDR_COURRIEL_HOST']

if 'FDR_COURRIEL_PORT' in os.environ:
    mail_port = int(os.environ['FDR_COURRIEL_PORT'])


def envoyer_courriel(message: dict):
    """
    Permet l'envoi de mails

    Args:
        message: Json contenant les informations nécessaire à l'envoi du mail.

    Returns:
        None en cas de reussite ou un tableau de mpiece_jointeessages d'erreur en cas d'echec

    Contenue de "message"
    ---------------------
    - "sujet": Obligatoire
        sujet du mail.

    - "de": Obligatoire
        Json contenant les informations sur l'expediteur du mail.
         - "nom": Optionnel
                  nom et prénoms de expédieur
         - "courriel": Obligatoire
                       courriel de l'expediteur
        exemple :
           {"nom": "expédieur", "courriel": "mail.expediteur@domaine.com"}

    - "pour": Obligatoire
        tableau de Json contenant les informations sur les destinataires du mail.
         - "nom": Optionnel
           nom et prénoms du destinataire
         - "courriel": Obligatoire
           courriel du destinataire
        exemple :
           [{"nom": "destinataires 1", "courriel": "mail.destinataires1@domaine.com"},
            {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],

    - "contenu_text": Optionnel
        message au format text.

    - "contenu_html": Optionnel
        Json contenant les informations html du message.
        contenu au format html.
         - "html" : message en html
         - "images" : liste des chemins des images contenues dans le html
         - exemple :
            {"html": contenu_html,
             "image": [{"file": repertoire+"logodnum.png"},
                       {"file": repertoire+"image1.jpeg"}],

    - "pieces_jointes":  Optionnel
        Liste des pieces jointes

    exemple :
    message = {
        "sujet": "sujet du mail",
        "de": {"nom": "expédieur", "courriel": "mail.expediteur@domaine.com"},
        "pour": [{"nom": "destinataires 1", "courriel": "mail.destinataires1@domaine.com"},
                 {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
        "contenu_text": contenu_text,
        "contenu_html": {"html": contenu_html,
                         "image": [{"file": repertoire+"logodnum.png"},
                                   {"file": repertoire+"image1.jpeg"}]},
        "pieces_jointes": []}

    """

    # test la validité des champs json
    erreurs = []
    resultat = json_utils.test_key_good_format(message, 'sujet', str, vide_interdit=True)
    if resultat is not None:
        erreurs.append(resultat)

    resultat = json_utils.test_key_good_format(message, 'de', dict, vide_interdit=True)
    if resultat is not None:
        erreurs.append(resultat)

    resultat = json_utils.test_key_good_format(message['de'], 'courriel', str, vide_interdit=True)
    if resultat is not None:
        erreurs.append("Dans ['de'] " + resultat)

    resultat = json_utils.test_key_good_format(message, 'pour', list, vide_interdit=True)
    if resultat is not None:
        erreurs.append(resultat)

    # expediteur
    msg = EmailMessage()
    msg['Subject'] = message['sujet']
    if 'nom' in message['de']:
        de = message['de']['nom'] + '<' + message['de']['courriel'] + '>'
    else:
        de = message['de']['courriel']
    msg['From'] = de

    # destinataires
    pour_list = []
    for pour in message['pour']:
        resultat = json_utils.test_key_good_format(pour, 'courriel', str, vide_interdit=True)
        if resultat is not None:
            erreurs.append(f'destinataire {len(pour_list)} - {resultat}')
            continue
        if 'nom' in pour:
            pour_list.append(pour['nom'] + '<' + pour['courriel'] + '>')
            de = ['courriel']
        else:
            pour_list.append(pour['courriel'])
    msg['To'] = pour_list

    # message texte
    if 'contenu_text' in message:
        msg.set_content(message['contenu_text'])

    # message html version.
    if 'contenu_html' in message:
        if message['contenu_html']['image']:
            # cas avec images inserées
            msgid = []
            for image in message['contenu_html']['image']:
                cid = make_msgid()
                image['cid'] = cid
                msgid.append(cid[1:-1])
            html = message['contenu_html']['html'].format(*msgid)
            msg.add_alternative(html, subtype='html')
            for image in message['contenu_html']['image']:
                try:
                    with open(image['file'], 'rb') as img:
                        msg.get_payload()[1].add_related(img.read(), 'image', 'jpeg', cid=image['cid'])
                        img.close()
                except Exception as e:
                    erreurs.append(f'html erreur image {e}')
        else:
            # cas sans images
            msg.add_alternative(message['contenu_html']['html'], subtype='html')

    # piece jointes
    if 'pieces_jointes' in message:
        for piece_jointe in message['pieces_jointes']:
            try:
                filemane = piece_jointe.split('/')
                filemane = filemane[len(filemane) - 1]

                ctype, encoding = mimetypes.guess_type(piece_jointe)
                if ctype is None or encoding is not None:
                    # No guess could be made, or the file is encoded (compressed), so
                    # use a generic bag-of-bits type.
                    ctype = 'application/octet-stream'
                maintype, subtype = ctype.split('/', 1)

                with open(piece_jointe, 'rb') as fp:
                    img_data = fp.read()
                    msg.add_attachment(img_data, maintype=maintype, subtype=subtype, filename=filemane)
            except Exception as e:
                erreurs.append(f'chargement piece jointe erreur - {e}')

    # si erreurs est vide, c'est qu'il n'y a pas eu d'erreurs
    if len(erreurs) != 0:
        return erreurs

    # Envoi du message via le serveur SMTP local.
    try:
        with smtplib.SMTP(mail_host, port=mail_port) as s:
            pour_list = []
            for pour in message['pour']:
                pour_list.append(pour['courriel'])
            s.sendmail(message['de']['courriel'], pour_list, msg.as_string())
            s.quit()
        return None
    except Exception as e:
        return [f'erreur envoie courriel. {e}']
