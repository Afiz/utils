import os

from utils.excel.excel_editor import ExcelEditor
import unittest

print("===================== TestExcelEditor ==============================")


class TestExcelEditor(unittest.TestCase):
    def setUp(self) -> None:
        repertoire = os.path.dirname(os.path.realpath(__file__))
        self.excel_editor = ExcelEditor("" + repertoire + "/data/empty-with-styles.xlsx")

    def test_sauvegarder(self):

        # When
        self.excel_editor.sauvegarder(self.excel_editor.filename)

        # Then
        filename = self.excel_editor.filename
        assert os.path.isfile(filename) is True

    def test_ajouter_titre(self):
        # When
        self.excel_editor.ajouter_titre("titre_test")

        # Then
        assert self.excel_editor.workbook.active.title == "titre_test"

    def test_inserer_lignes(self):
        # TODO tester qu'on n'ajoute pas des lignes en dehors des dimensions calculées
        # Given
        old_row_numbers = self.excel_editor.feuille.max_row
        rows_to_add = 5

        # When
        self.excel_editor.inserer_lignes(5, rows_to_add)
        # Then
        assert (
            self.excel_editor.workbook.active.max_row == old_row_numbers + rows_to_add
        )

    def test_inserer_cols(self):
        # TODO tester qu'on n'ajoute pas des colonnes en dehors des dimensions calculées
        # Given
        old_col_numbers = self.excel_editor.feuille.max_column
        cols_to_add = 5
        # When
        self.excel_editor.inserer_cols(1, cols_to_add)
        # Then
        assert (
            self.excel_editor.workbook.active.max_column
            == old_col_numbers + cols_to_add
        )

    def test_supprimer_lignes(self):
        # TODO tester qu'on n'ajoute pas des colonnes en dehors des dimensions calculées
        # Given
        old_row_numbers = self.excel_editor.feuille.max_row
        rows_to_delete = 1

        # When
        self.excel_editor.supprimer_lignes(2, rows_to_delete)

        # Then
        self.excel_editor.feuille.max_row == old_row_numbers - rows_to_delete

    def test_supprimer_cols(self):
        # TODO tester qu'on n'ajoute pas des colonnes en dehors des dimensions calculées
        # Given
        old_col_numbers = self.excel_editor.feuille.max_column
        cols_to_delete = 1

        # When
        self.excel_editor.supprimer_cols(1, cols_to_delete)

        # Then
        self.excel_editor.feuille.max_column == old_col_numbers - cols_to_delete

    def test_formater_cellule(self):
        # When

        # Then
        assert self.excel_editor.get_cellule("A1") == "TEST HERE"
