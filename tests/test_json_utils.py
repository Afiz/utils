import utils.json_utils
import unittest
import datetime

print("===================== TestJson ==============================")


class TestJson(unittest.TestCase):
    def setUp(self) -> None:
        pass

    def test_key_good_format(self):
        print("=== test_key_good_format ===")
        # cas 1 test type
        data = {"essai": "str"}
        key = "essai"
        _type = str
        resultat = utils.json_utils.test_key_good_format(data, key, _type)
        self.assertIsNone(resultat)

        _type = int
        resultat = utils.json_utils.test_key_good_format(data, key, _type)
        self.assertEqual(resultat,
                         "le champ essai n'est pas au bon format. reçu = <class 'str'>, attendu = <class 'int'>")

        # cas 2 test vide_interdit
        data = {"essai": ""}
        key = "essai"
        _type = str
        vide_interdit = False
        resultat = utils.json_utils.test_key_good_format(data, key, _type, vide_interdit=vide_interdit)
        self.assertIsNone(resultat)

        vide_interdit = True
        resultat = utils.json_utils.test_key_good_format(data, key, _type, vide_interdit=vide_interdit)
        self.assertEqual(resultat,
                         "le champ essai ne doit pas être vide")

        data = {"essai": None}
        _type = int
        vide_interdit = True
        resultat = utils.json_utils.test_key_good_format(data, key, _type, vide_interdit=vide_interdit)
        self.assertEqual(resultat,
                         "le champ essai ne doit pas être vide")

        # cas 3 test absent_interdit
        data = {}
        key = "essai"
        _type = str
        absent_interdit = False
        resultat = utils.json_utils.test_key_good_format(data, key, _type, absent_interdit=absent_interdit)
        self.assertIsNone(resultat)

        absent_interdit = True
        resultat = utils.json_utils.test_key_good_format(data, key, _type, absent_interdit=absent_interdit)
        self.assertEqual(resultat,
                         "le champ essai doit être présent")

        # cas 4 test limite
        data = {"essai": "str"}
        key = "essai"
        _type = str
        size_max = 3
        size_min = 3
        resultat = utils.json_utils.test_key_good_format(data, key, _type, size_max=size_max, size_min=size_min)
        self.assertIsNone(resultat)

        size_max = 2
        size_min = 0
        resultat = utils.json_utils.test_key_good_format(data, key, _type, size_max=size_max, size_min=size_min)
        self.assertEqual(resultat,
                         "le champ essai est trop long (3 pour un max de 2)")

        size_max = 1024
        size_min = 4
        resultat = utils.json_utils.test_key_good_format(data, key, _type, size_max=size_max, size_min=size_min)
        self.assertEqual(resultat,
                         "le champ essai est trop court (3 pour un min de 4)")
        print("terminé")

    def test_convert_all_datetime(self):
        print("=== test_convert_all_datetime ===")

        data = [
            {'key': 1, 'value': 'a', 'date': datetime.datetime(2020, 5, 13)},
            {'bool': True, 'object': {
                'date': datetime.datetime(2020, 5, 13),
                'list': [
                    {'id': 1, 'date': datetime.datetime(2020, 5, 13, 10, 35, 10, 20)},
                    {'id': 2, 'date': datetime.datetime(2021, 5, 13)},
                ]
            }
             }
        ]

        expected_data= [
            {'key': 1, 'value': 'a', 'date': '13/05/2020'},
            {'bool': True, 'object': {
                'date': '13/05/2020',
                'list': [
                    {'id': 1, 'date': '13/05/2020'},
                    {'id': 2, 'date': '13/05/2021'},
                ]
            }
             }
        ]

        expected_data2 = [
            {'key': 1, 'value': 'a', 'date': '2020-05-13 00:00:00.000000'},
            {'bool': True, 'object': {
                'date': '2020-05-13 00:00:00.000000',
                'list': [
                    {'id': 1, 'date': '2020-05-13 10:35:10.000020'},
                    {'id': 2, 'date': '2021-05-13 00:00:00.000000'},
                ]
            }
             }
        ]

        self.assertEqual(expected_data, utils.json_utils.convert_all_datetime(data))

        self.assertEqual(expected_data2, utils.json_utils.convert_all_datetime(data, "%Y-%m-%d %H:%M:%S.%f"))

        print("terminé")
