import os

import utils.courriel
import unittest

print("===================== TestCourriel ==============================")


class TestCourriel(unittest.TestCase):
    def setUp(self) -> None:
        pass

    @unittest.skip("serveur smtp non configuré dans la ci + manque adresse fonctionnelle pour testes")
    def test_envoyer_courriel(self):
        print("=== test_envoyer_courriel ===")

        repertoire = "" + os.path.dirname(os.path.realpath(__file__)) + "/data/"
        contenu_text = """\
Bonjour,
Ceci est un essai de mail"""
        contenu_html = """\
<html>
<head></head>
<body>
    <p>Salut!</p>
    <p>Ceci est un essai de mail
       <a href="http://www.google.com"> lien google</a>
    </p>
    avec une image
    <img src="cid:{}"/>
    avec une autre image
    <img src="cid:{}"/>
</body>
</html>"""

        # cas 1 nominale
        message = {
            "sujet": "test mail cas 1",
            "de": {"nom": "expédieur", "courriel": "mail.expediteur@domaine.com"},
            "pour": [{"nom": "Patrice Belmonte", "courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
            "contenu_text": contenu_text,
            "contenu_html": {"html": contenu_html,
                             "image": [{"file": repertoire+"logodnum.png"},
                                       {"file": repertoire+"image1.jpeg"}]},
            "pieces_jointes": []
        }
        resultat = utils.courriel.envoyer_courriel(message)
        self.assertIsNone(resultat)

        # cas 2 nominale avec nom vide
        message = {
            "sujet": "test mail cas 2 - avec nom vide",
            "de": {"nom": "", "courriel": "mail.expediteur@domaine.com"},
            "pour": [{"nom": "", "courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
            "contenu_text": contenu_text
        }
        resultat = utils.courriel.envoyer_courriel(message)
        self.assertIsNone(resultat)

        # cas 3 nominale sans nom
        message = {
            "sujet": "test mail cas 3 - sans nom",
            "de": {"courriel": "mail.expediteur@domaine.com"},
            "pour": [{"courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
            "contenu_text": contenu_text,
        }
        resultat = utils.courriel.envoyer_courriel(message)
        self.assertIsNone(resultat)

        # cas 4 erreur - sans courriel
        message = {
            "sujet": "test mail cas 4 - sans courriel",
            "de": {"courriel": ""},
            "pour": [{"courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2"}],
            "contenu_text": contenu_text,
        }
        resultat = utils.courriel.envoyer_courriel(message)
        resultat = "{}, {}".format(resultat[0], resultat[1])
        self.assertEqual(resultat, "Dans ['de'] le champ courriel ne doit pas être vide, destinataire 1 - le champ courriel doit être présent")

        # cas 5 erreur - sans sujet
        message = {
            "sujet": "",
            "de": {"courriel": "mail.expediteur@domaine.com"},
            "pour": [{"courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
            "contenu_text": contenu_text,
        }
        resultat = utils.courriel.envoyer_courriel(message)
        self.assertEqual(resultat[0], "le champ sujet ne doit pas être vide")

        # cas 6 erreur - connection smtp
        utils.courriel.mail_port = 225
        message = {
            "sujet": "test mail cas 6 erreur - connection smtp",
            "de": {"courriel": "mail.expediteur@domaine.com"},
            "pour": [{"courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
            "contenu_text": contenu_text,
        }
        resultat = utils.courriel.envoyer_courriel(message)
        self.assertEqual(resultat[0], "erreur envoie courriel. [Errno 111] Connection refused")

        # cas 7 erreur - html manque image
        utils.courriel.mail_port = 25
        message = {
            "sujet": "cas 7 erreur - html manque image",
            "de": {"nom": "expédieur", "courriel": "mail.expediteur@domaine.com"},
            "pour": [{"nom": "Patrice Belmonte", "courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
            "contenu_text": contenu_text,
            "contenu_html": {"html": contenu_html,
                             "image": [{"file": repertoire+"logodnum.png"},
                                       {"file": repertoire+"imag1.jpeg"}]},
            "pieces_jointes": []
        }
        resultat = utils.courriel.envoyer_courriel(message)
        test = "html erreur image [Errno 2]"
        resultat = resultat[0]
        self.assertEqual(resultat[:len(test)], test)

        # cas 8 - html trop d'images
        utils.courriel.mail_port = 25
        message = {
            "sujet": "cas 8 - html trop d'images",
            "de": {"nom": "expédieur", "courriel": "mail.expediteur@domaine.com"},
            "pour": [{"nom": "Patrice Belmonte", "courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
            "contenu_text": contenu_text,
            "contenu_html": {"html": contenu_html,
                             "image": [{"file": repertoire+"logodnum.png"},
                                       {"file": repertoire+"image1.jpeg"},
                                       {"file": repertoire+"image1.jpeg"}]},
            "pieces_jointes": []
        }
        resultat = utils.courriel.envoyer_courriel(message)
        self.assertIsNone(resultat)

        # cas 9 nominal - piece jointe
        utils.courriel.mail_port = 25
        message = {
            "sujet": "cas 9 nominal - piece jointe",
            "de": {"nom": "expédieur", "courriel": "mail.expediteur@domaine.com"},
            "pour": [{"nom": "Patrice Belmonte", "courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
            "contenu_text": contenu_text,
            "pieces_jointes": [repertoire+"logodnum.png", repertoire+"image1.jpeg", repertoire+"empty-with-styles.xlsx"]
        }
        resultat = utils.courriel.envoyer_courriel(message)
        self.assertIsNone(resultat)

        # cas 10 nominal - piece jointe nom trouvée
        utils.courriel.mail_port = 25
        message = {
            "sujet": "cas 9 nominal - piece jointe",
            "de": {"nom": "expédieur", "courriel": "mail.expediteur@domaine.com"},
            "pour": [{"nom": "Patrice Belmonte", "courriel": "patrice.belmonte@interieur.gouv.fr"},
                     {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
            "contenu_text": contenu_text,
            "pieces_jointes": [repertoire+"absent"]
        }
        resultat = utils.courriel.envoyer_courriel(message)
        test = "chargement piece jointe erreur - [Errno 2] No such file or directory:"
        resultat = resultat[0]
        self.assertEqual(resultat[:len(test)], test)
        print("terminé")
