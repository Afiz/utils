# Module d'utilitaires

## Envoie de mails

envoyer_courriel(message: dict):

- Arguments:
  - message: Json contenant les informations nécessaire à l'envoi du mail.
      - "sujet" Obligatoire : sujet du mail.

      - "de" Obligatoire : Json contenant les informations sur l'expediteur du mail.
          - "nom": Optionnel
            nom et prénoms de expédieur
          - "courriel": Obligatoire
            courriel de l'expediteur
            exemple :
            {"nom": "expédieur", "courriel": "mail.expediteur@domaine.com"}

      - "pour" Obligatoire : tableau de Json contenant les informations sur les destinataires du mail.
          - "nom": Optionnel
            nom et prénoms du destinataire
          - "courriel": Obligatoire
            courriel du destinataire
            exemple :
            [{"nom": "destinataires 1", "courriel": "mail.destinataires1@domaine.com"},
            {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],

      - "contenu_text" Optionnel:  message au format text.

      - "contenu_html" Optionnel:  Json contenant les informations html du message.
        contenu au format html:
          - "html" : message en html
          - "images" : liste des chemins des images contenues dans le html
          - exemple :
            ```code
            {"html": contenu_html,
            "image": [{"file": repertoire+"logodnum.png"},
                      {"file": repertoire+"image1.jpeg"}]
            ```
      - "pieces_jointes" Optionnel: Liste des pieces jointes

- Retourne:
        None en cas de reussite ou un tableau de mpiece_jointeessages d'erreur en cas d'echec

- Exemple :
  ```code
    message = {
        "sujet": "sujet du mail",
        "de": {"nom": "expédieur", "courriel": "mail.expediteur@domaine.com"},
        "pour": [{"nom": "destinataires 1", "courriel": "mail.destinataires1@domaine.com"},
                 {"nom": "destinataires 2", "courriel": "mail.destinataires2@domaine.com"}],
        "contenu_text": contenu_text,
        "contenu_html": {"html": contenu_html,
                         "image": [{"file": repertoire+"logodnum.png"},
                                   {"file": repertoire+"image1.jpeg"}]},
        "pieces_jointes": []}
  ```

# Tester le format d'une clé d'un json
def test_key_good_format(data, key, _type, vide_interdit=True, absent_interdit=True, size_max=1024, size_min=0):
- Arguments:
  - data: json à tester
  - key: cle à tester
  - vide_interdit: si True la cle doit ne doit pas être à none, ni une string vide
  - absent_interdit: si True la cle doit être presente
  - size_max: taille maximale d'une chaine de caractére
  - size_min: taille minimale d'une chaine de caractére
- Retourne:
  - None en cas de reussite ou un message d'erreur en cas d'echec

   